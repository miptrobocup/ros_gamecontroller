#include "ros/ros.h"
#include "std_msgs/String.h"

#include "GameControllerData_fwd.h"

void Callback(const std_msgs::String::ConstPtr& msg)
{
  ROS_INFO("Received: [%ld]", msg->data.size());
  auto data = DeserializeGCDataFromString(msg->data);
  /// Look for GameControlState
  auto state = static_cast<GameState>(data.state);
  ROS_INFO("Current GameControllerState: [%s]", toString(state));
}


int main(int argc, char **argv) {
 
  ros::init(argc, argv, "GCTopicListener");
  ros::NodeHandle n;
  auto sub = n.subscribe("GameControllerTopic", 1000, Callback);
  ros::spin();
  return 0;
}
