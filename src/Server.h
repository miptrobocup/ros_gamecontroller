#pragma once

#include "GameControllerData_fwd.h"

#include <string>
#include <memory>

class Server
{
public:
  Server();

  // Blocking method
  void ReceiveGCData();

  // Prints all GC data to ostream in pretty format
  void PrintGCDataToOstream(std::ostream &);

  // Serializes GC data to binary string (simply uses it as a buffer)
  std::string SerializeGCDataToString();

  // This is the only field we need
  GameState GetGameState() const;

  void SendReturnData();

private:
  class Impl;
  std::shared_ptr<Impl> pimpl_;
};