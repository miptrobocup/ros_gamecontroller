#include "Server.h"

#include <stdexcept>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/wait.h>

#include <iostream>
#include <memory>

#define BUF_SIZE 1000
#define PORT 3838
#define FLAGS_RECV_FROM 0
#define FLAGS_SEND_TO 0


class Server::Impl
{
  public:

  Impl(); 
  ~Impl() = default;

  void ReceiveGCData();

  void PrintGCDataToOstream(std::ostream &);
  std::string SerializeGCDataToString();
  GameState GetGameState() const;

  void SendReturnData();

private:
  void CheckBufAndCopy();
  void CreateSocket();
  void BindSocket();
  void ServAddressConfig();

private:
  RoboCupGameControlData receiveBuf;
  RoboCupGameControlData gameControlData;
  RoboCupGameControlReturnData returnData;

  int sockfd = 0;
  struct sockaddr_in servaddr, cliaddr;
};


Server::Impl::Impl()
{
  CreateSocket();
  ServAddressConfig();
  BindSocket();
}


void Server::Impl::CreateSocket()
{
  sockfd = socket(PF_INET, SOCK_DGRAM, 0);
}


void Server::Impl::ServAddressConfig()
{
  bzero(&cliaddr, sizeof(cliaddr));
  servaddr.sin_family = AF_INET;
  servaddr.sin_port = htons(PORT);
  servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
}


void Server::Impl::BindSocket()
{
  if(bind(sockfd, (struct sockaddr *) &servaddr, sizeof(cliaddr)) < 0)
  {
    throw std::runtime_error("Error while binding the socket.");
    close(sockfd);
  }
}


void Server::Impl::ReceiveGCData()
{
  int clilen = sizeof(cliaddr);
  int n = recvfrom(sockfd, &receiveBuf, sizeof(receiveBuf) - 1, FLAGS_RECV_FROM, (struct sockaddr *) &cliaddr, (socklen_t *) &clilen);

  if (n < 0)
    throw std::runtime_error("Error while receiving the data.");

  CheckBufAndCopy();
}


void Server::Impl::CheckBufAndCopy()
{
  if(strcmp(receiveBuf.header, "RGme"))
  {
    std::cout << "Correct message received\n";
    gameControlData = receiveBuf;
  }
  else
    std::cout << "Incorrect message received\n";
}


void Server::Impl::SendReturnData()
{
  if(sendto(sockfd, &returnData, sizeof(returnData), FLAGS_SEND_TO, (struct sockaddr *) &cliaddr, sizeof(cliaddr)) < 0)
  {
    throw std::runtime_error("Error while sending data to GC.");
  }
}


void Server::Impl::PrintGCDataToOstream(std::ostream & ss)
{

  ss << "Header: " << gameControlData.header << '\n';
  ss << "Pack num: " << gameControlData.packetNumber << '\n';
  ss << "State: " << gameControlData.state << '\n';

  ss << "Version: " << gameControlData.version << '\n';
  ss << "Players per team: " << gameControlData.playersPerTeam << '\n';
  ss << "CompetitionPhase: " << gameControlData.competitionPhase << '\n';
  ss << "CompetitionType: " << gameControlData.competitionType << '\n';
  ss << "GamePhase: " << gameControlData.gamePhase << '\n';
  ss << "Set play: " << gameControlData.setPlay << '\n';
  ss << "First half: " << gameControlData.firstHalf << '\n';
  ss << "Kicking team: " << gameControlData.kickingTeam << '\n';
  ss << "Seconds remaining: " << gameControlData.secsRemaining << '\n';
  ss << "Secondary time: " << gameControlData.secondaryTime << '\n';

  ss << "Team 1 info:" << '\n';
  ss << "Number: " << gameControlData.teams[0].teamNumber << '\n';
  ss << "Colour: " << gameControlData.teams[0].teamColour << '\n';
  ss << "Score: " << gameControlData.teams[0].score << '\n';
  ss << "Penalty shot: " << gameControlData.teams[0].penaltyShot << '\n';
  ss << "Signle shots: " << gameControlData.teams[0].singleShots << '\n';


  RobotInfo * s;

  s = gameControlData.teams[0].players;
  ss << "Players info:\n";
  ss 
    << "Penalty: " 
    << s[0].penalty << '\t' << s[1].penalty << '\t' << s[2].penalty << '\t'
    << s[3].penalty << '\t' << s[4].penalty << '\t' << s[5].penalty << '\n';
  ss 
    << "Secs till unpen: " 
    << s[0].secsTillUnpenalised << '\t' << s[1].secsTillUnpenalised << '\t' << s[2].secsTillUnpenalised << '\t'
    << s[3].secsTillUnpenalised << '\t' << s[4].secsTillUnpenalised << '\t' << s[5].secsTillUnpenalised << '\n';

  ss << "Team 2 info:\n" <<'\n';
  ss << "Number: " << gameControlData.teams[1].teamNumber << '\n';
  ss << "Colour: " << gameControlData.teams[1].teamColour << '\n';
  ss << "Score: " << gameControlData.teams[1].score << '\n';
  ss << "Penalty shot: " << gameControlData.teams[1].penaltyShot << '\n';
  ss << "Signle shots: " << gameControlData.teams[1].singleShots << '\n';

  s = gameControlData.teams[1].players;
  ss << "Players info:\n";
  ss 
    << "Penalty: "
    << s[0].penalty << '\t' << s[1].penalty << '\t' << s[2].penalty << '\t'
    << s[3].penalty << '\t' << s[4].penalty << '\t' << s[5].penalty << '\n';
  ss <<
      "Secs till unpen: "
      << s[0].secsTillUnpenalised << '\t' << s[1].secsTillUnpenalised << '\t' << s[2].secsTillUnpenalised << '\t'
      << s[3].secsTillUnpenalised << '\t' << s[4].secsTillUnpenalised << '\t' << s[5].secsTillUnpenalised << '\n';
}

std::string Server::Impl::SerializeGCDataToString()
{
    return ::SerializeGCDataToString(gameControlData);
}

GameState Server::Impl::GetGameState() const
{
  return static_cast<GameState>(gameControlData.state);
}


Server::Server() : pimpl_(std::make_shared<Server::Impl>())
{}


void Server::ReceiveGCData()
{
  pimpl_->ReceiveGCData();
}


void Server::PrintGCDataToOstream(std::ostream & ss)
{
  pimpl_->PrintGCDataToOstream(ss);
}


std::string Server::SerializeGCDataToString()
{
  return pimpl_->SerializeGCDataToString();
}


GameState Server::GetGameState() const
{
  return pimpl_->GetGameState();
}


void Server::SendReturnData() 
{
  pimpl_->SendReturnData();
}
