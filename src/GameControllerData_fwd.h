#pragma once

#include "OriginalGCFiles/RobocupGameControllerData.h"
#include "OriginalGCFiles/SPLStandartMessage.h"

#include <cstring>
#include <stdexcept>
#include <string>
#include <cassert>


/// RobocupGameControlData is POD struct
inline std::string SerializeGCDataToString(RoboCupGameControlData data)
{
    const size_t data_size = sizeof(RoboCupGameControlData);
    std::string buffer;
    buffer.resize(data_size);
    ::memcpy(&buffer[0], &data, data_size);
    return buffer;
}

inline RoboCupGameControlData DeserializeGCDataFromString(const std::string & data)
{
    const size_t data_size = sizeof(RoboCupGameControlData);
    assert(data.size() >= data_size);
    RoboCupGameControlData answer;
    ::memcpy(&answer, &data[0], data_size);
    return answer;
}


enum class GameState : uint8_t
{
    INITIAL = 0,
    READY = 1,
    SET = 2,
    PLAYING = 3,
    FINISHED = 4,
    PENALIZED = 5
};

inline const char * toString(GameState state)
{
    switch (state)
    {
    case GameState::INITIAL: return "INITIAL";
    case GameState::READY: return "READY";
    case GameState::SET: return "SET";
    case GameState::PLAYING: return "PLAYING";
    case GameState::FINISHED: return "FINISHED";
    case GameState::PENALIZED: return "PENALIZED";
    default: throw std::runtime_error("Unknown GameState!");
    }
};
