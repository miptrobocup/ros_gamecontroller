
#include <sstream>
#include "ros/ros.h"
#include "std_msgs/String.h"

#include "Server.h"

int main(int argc, char **argv) {

  ros::init(argc, argv, "GCTopicWriter");
  ros::NodeHandle n;
  auto chatter_pub = n.advertise<std_msgs::String>("GameControllerTopic", 1000);

  ros::Rate loop_rate(10);

  Server server;

  int count = 0;
  while (ros::ok())
  {
    server.ReceiveGCData();

    std_msgs::String msg;
    msg.data = server.SerializeGCDataToString();

    /// This is a string represents serialized message, so it is not printable.
    ROS_INFO("%s", msg.data.c_str());

    chatter_pub.publish(msg);
    loop_rate.sleep();
    ++count;
  }


  return 0;
}
