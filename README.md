# ros_gamecontroller

### Overview:

This package provides two nodes called `GCTopicWriter` and `GCTopicListener`.
First receives messages from `GameController` and pushes them into a topic called `GameControllerTopic` in a serialized binary format.
The latter simply reads from topic and prits messages to the display. 
The messages looks like this:
```
[ INFO] [1618790756.481630847]: Received: [54]
[ INFO] [1618790756.481705893]: Current GameControllerState: [SET]
[ INFO] [1618790760.475286352]: Received: [54]
[ INFO] [1618790760.475397712]: Current GameControllerState: [PLAYING]
[ INFO] [1618790760.987129655]: Received: [54]
[ INFO] [1618790763.928039268]: Current GameControllerState: [FINISHED]
```
While the `GameController` is launched on the other laptop connected to the same network.

### Details:

GameController uses broadcast address 192.168.1.255 and 3838 to perform data transfer.
It uses messages defined in `OriginalGCFiles/RobocupGameControllerData.h` which brought from original `GameController` repository.
In source code there is a class called `Server`, which is listens on a UDP socket and receives this type of message.
It has some other methods like to print data in pretty format, which is very useful in debugging.
Also it could serialize this message into a string.
According to the fact that this struct is POD (Plain Old Data) we used simple `::memcpy` function to copy the internals to the string inner buffer. 
This string is being send to the topic, and on the other side `GCTopicListener` deserializes it from string, gets the `GameState` field and prints it to the screen.

`Server` class uses pimpl idiom for faster builds.


### How to test:
- Launch original GameController on the other laptop connected to the same network.
- Execute these commands is separate terminals:
```
rosrun game_controller GCTopicWriter
rosrun game_controller GCTopicListener
```
You will see continious stream of messages in both terminals.

```bash
↳ $ rosrun game_controller GCTopicWriter
sockfd = 10
Correct message received
? INFO] [1618790686.234391632]: RGme
Correct message received
? INFO] [1618790686.746295006]: RGme
Correct message received
? INFO] [1618790687.258214319]: RGme
Correct message received
```

Note that because string in C language are null terminated (has \0 at the end) and header of the message is a string ("RGme"),
we can't display the other part of the message received from GameController.

```
↳ $ rosrun game_controller GCTopicListener
[ INFO] [1618790755.969832505]: Current GameControllerState: [SET]
[ INFO] [1618790756.481630847]: Received: [54]
[ INFO] [1618790756.481705893]: Current GameControllerState: [SET]
[ INFO] [1618790760.475286352]: Received: [54]
[ INFO] [1618790760.475397712]: Current GameControllerState: [PLAYING]
[ INFO] [1618790760.987129655]: Received: [54]
[ INFO] [1618790762.523717682]: Current GameControllerState: [FINISHED]
```